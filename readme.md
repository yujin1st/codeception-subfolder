### IDE doesn't run included tests standalone
https://youtrack.jetbrains.com/issue/WI-37664

Reproducing bug 
- run composer install
- configure ide for all codeception tests  (php default config file) - OK
- open folder/tests/functional/TestCest.php  
  and try run only this test file, that ide found itself - FAILS

console command, that works fine:
- run vendor/bin/codecept run functional -c folder/tests TestCest 
- run vendor/bin/codecept run -c folder/tests TestCest
